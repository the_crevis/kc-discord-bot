package com.kelvinconnect.discord.login;

import de.btobastian.javacord.DiscordApi;

/**
 * Interface for login classes
 *
 * Created by Adam on 14/03/2017.
 */
public interface Login {

    DiscordApi login();
}
