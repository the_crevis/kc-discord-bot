package com.kelvinconnect.discord;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PersistenceManager {
    private static final String DELIMITER = ",";
    private static final String NEW_LINE = "\n";

    public boolean saveToCSV(String filePath, String[]... rows)
    {
        try(FileWriter fileWriter = new FileWriter(new File(filePath), true))
        {
            for(String[] row : rows)
            {
                for(String column : row)
                {
                    fileWriter.write(column);
                    fileWriter.write(DELIMITER);
                }
                fileWriter.write(NEW_LINE);
            }
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public List<String[]> loadFromCSV(String filePath)
    {
        try(Scanner scanner = new Scanner(new File(filePath))) {
            List<String[]> rows = new ArrayList<>();
            while (scanner.hasNext()) {
                String row = scanner.next();
                String[] columns = row.split(",");
                rows.add(columns);
            }
            return rows;
        } catch (FileNotFoundException e) {
            return Collections.emptyList();
        }
    }
}
